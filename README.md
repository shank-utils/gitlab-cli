## The "Why?"
- Because command line tools are fun
- Because [Go](https://golang.org) makes it super easy to create cross platform command line tools
- Becuase Gitlab is awesome
- Because creating issues, by navigating to the issues page and then entering the details, is slow and dumb


## What is it?
##### A command line tool to interact with [Gitlab](https://gitlab.com), currently focusing on working with issues against a project. 
Some things which it can currently do are:
- creating an issue
- assigning an issue to a member
- listing the project members
- adding / removing labels to an issue
- closing / reopening an issue

and some others...


## How do I install it?
- **Part 1 - Installation**
    - **Option 1** - If you have `go` installed on your computer:
        - `go get gitlab.com/shank/gitlab-cli/cmd/gitlab`
    - **Option 2** - If you do not have `go` installed on your computer:
        - Download the latest release binary for your operating system from [this](https://gitlab.com/shank/gitlab-cli/tags) page. (Yes I wish Gitlab had a release page feature like Github does, but it doesn't, so this is the best we get)
        - Add the downloaded binary to your `PATH`. (Google it, in case you do not know how to do this)
- **Part 2 - GITLAB_PRIVATE_TOKEN**
    - `gitlab-cli` uses your Gitlab Private Token to make the requests on your behalf. Create it on the account page under your profile [here](https://gitlab.com/profile/account)
    - Create an environment variable (on your computer) named GITLAB_PRIVATE_TOKEN and set its value to the private token you created above

Thats it !

## Where can I get more usage info for this tool?
[TODO - refer to the blog post about usage]


## Supported commands?
- issues
  - [x] list issues 
  - [x] create
  - [x] delete
  - [x] close / reopen
  - [x] assign to project member
  - [x] add label
  - [ ] open issue page in web browser
- members
  - [x] list project members
  - [ ] list members of current project's group
  - [ ] add member to project
- labels
  - [x] list labels
  - [x] create
  - [x] edit
  - [x] delete
- projects
  - [ ] create project


## If you are primarily targeting working with issues, why is it named "gitlab-cli"?
Currently this project is a result of "scratch your own itch" philosophy. However I do plan to add features to it (by considering the feature requests submitted) to make it a one stop cli client for Gitlab!

## How does it identify the default project if I do not specify a `--pid`?
It tries to get the project details be reading the remote url for the current git repository.


## I found a bug / I want to request a feature
Please log an [issue](https://gitlab.com/shank/gitlab-cli/issues) for both bugs and feature requests.


## Credits
`gitlab-cli` is written in [Go](https://golang.org). 
It stands on the shoulders of two great projects:
- [mow.cli](https://github.com/jawher/mow.cli) A versatile library for building CLI applications in Go
- [go-gitlab](https://github.com/xanzy/go-gitlab) A GitLab API client enabling Go programs to interact with GitLab in a simple and uniform way

These two do all the heavy lifting, enabling me to create this tool with minimal effort.
