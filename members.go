package gitlab_cli

import (
	"fmt"
	"log"

	gitlab "github.com/xanzy/go-gitlab"
)

func GetMemberIDForUsername(gl *gitlab.Client, pid interface{}, username string) int {
	members := GetProjectMembers(gl, pid)
	var memberId int
	for _, member := range members {
		if member.Username == username {
			memberId = member.ID
			break
		}
	}
	if memberId == 0 {
		log.Fatalln("No project member has the username:", username)
	}
	return memberId
}

func GetProjectMembers(gl *gitlab.Client, pid interface{}) []*gitlab.ProjectMember {
	members, _, err := gl.Projects.ListProjectMembers(pid, nil)
	if err != nil {
		log.Fatalln("error in getting the list of project members")
	}
	return members
}

func ListProjectMembers(pid interface{}) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()
	members := GetProjectMembers(gl, pid)

	// output the data
	table := DefaultTable()
	table.AddRow("#", "USERNAME", "NAME")
	for _, member := range members {
		table.AddRow(member.ID, member.Username, member.Name)
	}
	fmt.Println(table)
}
