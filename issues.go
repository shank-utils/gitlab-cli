package gitlab_cli

import (
	"fmt"
	"log"
	"strings"

	gitlab "github.com/xanzy/go-gitlab"
)

func ListProjectIssues(pid interface{}, state, assignee string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	// using pid and gitlab client get the issues
	opts := &gitlab.ListProjectIssuesOptions{}
	// state - opened, closed
	opts.State = gitlab.String(state)
	issues := GetProjectIssues(gl, pid, opts)

	// in case we are looking only for issues assigned to a user...
	if assignee != "" {
		filtered := []*gitlab.Issue{}
		for _, issue := range issues {
			if issue.Assignee.Username == assignee {
				filtered = append(filtered, issue)
			}
		}
		issues = filtered
	}

	// output the data
	table := DefaultTable()
	table.RightAlign(1)
	table.AddRow("#ID", "#IID", "TITLE", "STATUS", "ASSIGNED TO", "LABELS")
	for _, issue := range issues {
		table.AddRow(issue.ID, issue.IID, issue.Title, issue.State, issue.Assignee.Name, strings.Join(issue.Labels, ","))
	}
	fmt.Println(table)
}

func CreateProjectIssue(pid interface{}, title, assignee string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	opts := &gitlab.CreateIssueOptions{}
	opts.Title = gitlab.String(title)
	if assignee != "" {
		username := assignee
		// get the list of project members
		members := GetProjectMembers(gl, pid)
		// get the id of the member whose name matches
		var assigneeId int
		for _, member := range members {
			if member.Username == username {
				assigneeId = member.ID
				break
			}
		}
		// in case we dont have a member with that name, print an error and exit
		if assigneeId == 0 {
			log.Fatalln("No member found for username:", username)
		}
		opts.AssigneeID = &assigneeId
	}
	issue, _, err := gl.Issues.CreateIssue(pid, opts)
	fatalOnErr(err)
	fmt.Println("Issue created:", issue.IID)
}

func DeleteProjectIssues(pid interface{}, isGid bool, issueIds []int) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	var projIssues []*gitlab.Issue
	if !isGid {
		projIssues = GetProjectIssues(gl, pid, nil)
	}

	for _, issueId := range issueIds {
		if !isGid {
			// get the id for each issue, since the args are the iids only
			for _, issue := range projIssues {
				if issue.IID == issueId {
					issueId = issue.ID
					break
				}
			}
		}
		_, err := gl.Issues.DeleteIssue(pid, issueId)
		fatalOnErr(err)
		fmt.Printf("Issue #%d deleted\n", issueId)
	}
}

func CloseProjectIssues(pid interface{}, isGid bool, issueIds []int) {
	closeOrReopenProjectIssue(pid, isGid, issueIds, "close")
}

func ReopenProjectIssues(pid interface{}, isGid bool, issueIds []int) {
	closeOrReopenProjectIssue(pid, isGid, issueIds, "reopen")
}

func closeOrReopenProjectIssue(pid interface{}, isGid bool, issueIds []int, stateEvent string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	var projIssues []*gitlab.Issue
	if !isGid {
		projIssues = GetProjectIssues(gl, pid, nil)
	}
	opts := &gitlab.UpdateIssueOptions{}
	opts.StateEvent = gitlab.String(stateEvent)
	for _, issueId := range issueIds {
		if !isGid {
			// get the id for each issue, since the args are the iids only
			for _, issue := range projIssues {
				if issue.IID == issueId {
					issueId = issue.ID
					break
				}
			}
		}
		_, _, err := gl.Issues.UpdateIssue(pid, issueId, opts)
		fatalOnErr(err)
		fmt.Printf("Issue #%d %sed\n", issueId, stateEvent)
	}
}

func AssignProjectIssues(pid interface{}, isGid bool, issueIds []int, assignee string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	memberId := GetMemberIDForUsername(gl, pid, assignee)

	var projIssues []*gitlab.Issue
	if !isGid {
		projIssues = GetProjectIssues(gl, pid, nil)
	}

	for _, issueId := range issueIds {
		var finalIssueID = issueId
		if !isGid {
			// get the id for each issue, since the args are the iids only
			for _, issue := range projIssues {
				if issue.IID == issueId {
					finalIssueID = issue.ID
					break
				}
			}
			if finalIssueID == issueId {
				log.Fatalln("Issue with IID %d not found in the project")
			}
		}
		opts := &gitlab.UpdateIssueOptions{}
		opts.AssigneeID = &memberId
		_, _, err := gl.Issues.UpdateIssue(pid, finalIssueID, opts)
		fatalOnErr(err)
		fmt.Printf("Issue #%d assigned to %s\n", issueId, assignee)
	}
}

func AddLabelsToProjectIssues(pid interface{}, isGid bool, labels []string, issueIds []int) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	var projIssues []*gitlab.Issue
	if !isGid {
		projIssues = GetProjectIssues(gl, pid, nil)
	}

	for _, issueId := range issueIds {
		var finalIssueID = issueId
		if !isGid {
			// get the id for each issue, since the args are the iids only
			for _, issue := range projIssues {
				if issue.IID == issueId {
					finalIssueID = issue.ID
					break
				}
			}
			if finalIssueID == issueId {
				log.Fatalln("Issue with IID %d not found in the project")
			}
		}
		opts := &gitlab.UpdateIssueOptions{}
		opts.Labels = labels
		_, _, err := gl.Issues.UpdateIssue(pid, finalIssueID, opts)
		fatalOnErr(err)
		fmt.Printf("Labels: #%s added to issue: %d\n", strings.Join(labels, ","), issueId)
	}
}

func GetProjectIssues(gl *gitlab.Client, pid interface{}, opts *gitlab.ListProjectIssuesOptions) []*gitlab.Issue {
	issues, _, err := gl.Issues.ListProjectIssues(pid, opts)
	if err != nil {
		log.Fatalln("error in getting list of issues for the project\n", err)
	}
	return issues
}
