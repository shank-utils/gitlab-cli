package gitlab_cli

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/gosuri/uitable"
	gitlab "github.com/xanzy/go-gitlab"
)

func fatalOnErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func GetDefaultPID() string {
	return GetPID("origin")
}

func GetPID(remote string) string {
	// remote url is of the form `https://gitlab.com/shank/gitlab.git` or `git@gitlab.com:shank/gitlab.git`
	url, err := exec.Command("git", "remote", "get-url", remote).Output()
	fatalOnErr(err)
	remoteUrl := strings.TrimSpace(string(url))
	remoteUrl = strings.TrimSuffix(remoteUrl, ".git")
	parts := strings.Split(remoteUrl, "/")
	// in case the remote is of the form git@gitlab.com:user/repo.git
	// trim the parts[-2] string upto and including the :
	grp := parts[len(parts)-2]
	if strings.Contains(grp, ":") {
		grp = strings.Split(grp, ":")[1]
	}
	repoName := parts[len(parts)-1]
	return fmt.Sprintf("%s/%s", grp, repoName)
}

func GetTokenFromEnv() string {
	token, exists := os.LookupEnv("GITLAB_PRIVATE_TOKEN")
	if !exists {
		log.Fatalln("env variable titled GITLAB_PRIVATE_TOKEN not found")
	}
	return token
}

func GetGitlabInstance() *gitlab.Client {
	// read the token and setup the gitlab instance
	token := GetTokenFromEnv()
	// setup the Gitlab instance for the program
	return gitlab.NewClient(nil, token)
}

func DefaultTable() *uitable.Table {
	table := uitable.New()
	table.MaxColWidth = 50
	table.Wrap = true
	table.RightAlign(0)
	return table
}
