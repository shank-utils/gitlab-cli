package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/jawher/mow.cli"
	gcli "gitlab.com/shank/gitlab-cli"
)

func main() {
	glcli := cli.App("gitlab", "Gitlab-CLI")
	glcli.Version("v version", "gitlab-cli 0.1.1")

	setupMembersCmd(glcli)
	setupIssuesCmd(glcli)
	setupLabelsCmd(glcli)

	glcli.Run(os.Args)
}

// MEMBERS
func setupMembersCmd(glcli *cli.Cli) {
	glcli.Command("members", "Work with project members", func(membersCmd *cli.Cmd) {
		setupMembersCmdListMembers(membersCmd)
	})
}

// list members
func setupMembersCmdListMembers(membersCmd *cli.Cmd) {
	membersCmd.Command("list", "list the project members", func(listCmd *cli.Cmd) {
		listCmd.Spec = "[--pid]"

		var pid = listCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")

		listCmd.Action = func() {
			gcli.ListProjectMembers(*pid)
		}
	})
}

// ISSUES
func setupIssuesCmd(glcli *cli.Cli) {
	glcli.Command("issues", "Work with project issues", func(issuesCmd *cli.Cmd) {
		setupIssuesCmdListIssues(issuesCmd)
		setupIssuesCmdCreateIssue(issuesCmd)
		setupIssuesCmdDeleteIssues(issuesCmd)
		setupIssuesCmdCloseIssues(issuesCmd)
		setupIssuesCmdReopenIssues(issuesCmd)
		setupIssuesCmdAssignIssues(issuesCmd)
		setupIssuesCmdAddLabelsToIssues(issuesCmd)
	})
}

// list issues
func setupIssuesCmdListIssues(issuesCmd *cli.Cmd) {
	issuesCmd.Command("list", "list the project issues", func(listCmd *cli.Cmd) {
		listCmd.Spec = "[--pid] [--state | (--open | --closed)...] [--assignee]"

		var pid = listCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		var stateOpt = listCmd.StringOpt("s state", "opened,closed", "state of issues")
		var assignee = listCmd.StringOpt("a assignee", "", "username of the member")
		var open = listCmd.BoolOpt("o open", false, "list open issues")
		var closed = listCmd.BoolOpt("c closed", false, "list closed issues")

		listCmd.Action = func() {
			var state string
			var states []string
			if *open || *closed {
				if *open {
					states = append(states, "opened")
				}
				if *closed {
					states = append(states, "closed")
				}
				state = strings.Join(states, ",")
			} else {
				state = *stateOpt
			}
			gcli.ListProjectIssues(*pid, state, *assignee)
		}
	})
}

// create issue
func setupIssuesCmdCreateIssue(issuesCmd *cli.Cmd) {
	issuesCmd.Command("create", "Create issue", func(createCmd *cli.Cmd) {
		createCmd.Spec = "[--pid] (--title | TITLE) [--assignee]"

		pid := createCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		titleOpt := createCmd.StringOpt("t title", "", "title of the issue")
		titleArg := createCmd.StringArg("TITLE", "", "title of the issue")
		assignee := createCmd.StringOpt("a assignee", "", "username of the dev to whom the issue is to be assigned")

		createCmd.Action = func() {
			var title string
			if *titleOpt != "" {
				title = *titleOpt
			} else {
				title = *titleArg
			}

			if title == "" {
				fmt.Println("Please enter a non-empty title for label")
				os.Exit(1)
			}
			gcli.CreateProjectIssue(*pid, title, *assignee)
		}
	})
}

// delete issues
func setupIssuesCmdDeleteIssues(issuesCmd *cli.Cmd) {
	issuesCmd.Command("delete", "Delete issues", func(deleteCmd *cli.Cmd) {
		deleteCmd.Spec = "[--pid] [--gid] ISSUEID..."

		pid := deleteCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		gid := deleteCmd.BoolOpt("gid", false, "Flag for considering the issue ids provided in arguments as global issue ids and not project specific issue ids")
		issueIds := deleteCmd.IntsArg("ISSUEID", nil, "ids of issues to be deleted")

		deleteCmd.Action = func() {
			gcli.DeleteProjectIssues(*pid, *gid, *issueIds)
		}
	})
}

// close issue
func setupIssuesCmdCloseIssues(issuesCmd *cli.Cmd) {
	issuesCmd.Command("close", "Close Issues", func(closeCmd *cli.Cmd) {
		closeCmd.Spec = "[--pid] [--gid] ISSUEID..."

		pid := closeCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		gid := closeCmd.BoolOpt("gid", false, "Flag for considering the issue ids provided in arguments as global issue ids and not project specific issue ids")
		issueIds := closeCmd.IntsArg("ISSUEID", nil, "ids of issues to be deleted")

		closeCmd.Action = func() {
			gcli.CloseProjectIssues(*pid, *gid, *issueIds)
		}
	})
}

// reopen issue
func setupIssuesCmdReopenIssues(issuesCmd *cli.Cmd) {
	issuesCmd.Command("reopen", "Reopen Issues", func(reopenCmd *cli.Cmd) {
		reopenCmd.Spec = "[--pid] [--gid] ISSUEID..."

		pid := reopenCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		gid := reopenCmd.BoolOpt("gid", false, "Flag for considering the issue ids provided in arguments as global issue ids and not project specific issue ids")
		issueIds := reopenCmd.IntsArg("ISSUEID", nil, "ids of issues to be deleted")

		reopenCmd.Action = func() {
			gcli.ReopenProjectIssues(*pid, *gid, *issueIds)
		}
	})
}

// assign issue
func setupIssuesCmdAssignIssues(issuesCmd *cli.Cmd) {
	issuesCmd.Command("assign", "Assign Issues", func(assignCmd *cli.Cmd) {
		assignCmd.Spec = "[--pid] ASSIGNEE [--gid] ISSUEID..."

		pid := assignCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		gid := assignCmd.BoolOpt("gid", false, "Flag for considering the issue ids provided in arguments as global issue ids and not project specific issue ids")
		issueIds := assignCmd.IntsArg("ISSUEID", nil, "ids of issues to be deleted")
		assignee := assignCmd.StringArg("ASSIGNEE", "", "username of the dev to whom the issue is to be assigned")

		assignCmd.Action = func() {
			gcli.AssignProjectIssues(*pid, *gid, *issueIds, *assignee)
		}
	})
}

// add label to issue
func setupIssuesCmdAddLabelsToIssues(issuesCmd *cli.Cmd) {
	issuesCmd.Command("addlabel", "Add label to Issues", func(addLabelCmd *cli.Cmd) {
		addLabelCmd.Spec = "[--pid] (LABELS) [--gid] ISSUEID..."

		pid := addLabelCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		gid := addLabelCmd.BoolOpt("gid", false, "Flag for considering the issue ids provided in arguments as global issue ids and not project specific issue ids")
		labels := addLabelCmd.StringArg("LABELS", "", "Labels to be added to the issues")
		issueIds := addLabelCmd.IntsArg("ISSUEID", nil, "ids of issues to which the labels are to be added")

		addLabelCmd.Action = func() {
			if *labels == "" {
				fmt.Println("Please enter a non-empty label to be added to issue")
				os.Exit(1)
			}
			gcli.AddLabelsToProjectIssues(*pid, *gid, strings.Split(*labels, ","), *issueIds)
		}
	})
}

// LABELS
func setupLabelsCmd(glcli *cli.Cli) {
	glcli.Command("labels", "Work with labels in this project", func(labelsCmd *cli.Cmd) {
		setupLabelsCmdListLabels(labelsCmd)
		setupLabelsCmdCreateLabel(labelsCmd)
		setupLabelsCmdEditLabel(labelsCmd)
		setupLabelsCmdDeleteLabel(labelsCmd)

	})
}

// list labels
func setupLabelsCmdListLabels(labelsCmd *cli.Cmd) {
	labelsCmd.Command("list", "list the project labels", func(listCmd *cli.Cmd) {
		listCmd.Spec = "[--pid]"
		var pid = listCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		listCmd.Action = func() {
			gcli.ListProjectLabels(*pid)
		}
	})
}

// create label
func setupLabelsCmdCreateLabel(labelsCmd *cli.Cmd) {
	labelsCmd.Command("create", "Create label", func(createCmd *cli.Cmd) {
		createCmd.Spec = "[--pid] (--title | TITLE) (--color)"

		var pid = createCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		var titleOpt = createCmd.StringOpt("t title", "", "title of the label")
		var titleArg = createCmd.StringArg("TITLE", "", "title of the label")
		var color = createCmd.StringOpt("c color", "", "color of the label, in hex notation")

		createCmd.Action = func() {
			var title string
			if *titleOpt != "" {
				title = *titleOpt
			} else {
				title = *titleArg
			}

			if title == "" {
				fmt.Println("Please enter a non-empty title for label")
				os.Exit(1)
			}
			if *color == "" {
				fmt.Println("Please enter a color for label")
				os.Exit(1)
			}

			gcli.CreateProjectLabel(*pid, title, *color)
		}
	})
}

func setupLabelsCmdEditLabel(labelsCmd *cli.Cmd) {
	// edit label
	labelsCmd.Command("edit", "Edit label", func(createCmd *cli.Cmd) {
		createCmd.Spec = "[--pid] TITLE ((--new-title | --color | --desc)...)"

		pid := createCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		title := createCmd.StringArg("TITLE", "", "existing title of the label")
		newTitle := createCmd.StringOpt("t new-title", "", "new title of the label")
		newColor := createCmd.StringOpt("c color", "", "color of the label, in hex notation")
		newDesc := createCmd.StringOpt("d desc", "", "new description of the label")

		createCmd.Action = func() {
			gcli.EditProjectLabel(*pid, *title, *newTitle, *newColor, *newDesc)
		}
	})
}

func setupLabelsCmdDeleteLabel(labelsCmd *cli.Cmd) {
	// delete label
	labelsCmd.Command("delete", "Delete label", func(createCmd *cli.Cmd) {
		createCmd.Spec = "[--pid] TITLE"

		pid := createCmd.StringOpt("p pid", "", "<namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'")
		title := createCmd.StringArg("TITLE", "", "existing title of the label")

		createCmd.Action = func() {
			gcli.DeleteProjectLabel(*pid, *title)
		}
	})
}
