package gitlab_cli

import (
	"fmt"
	"log"

	gitlab "github.com/xanzy/go-gitlab"
)

func ListProjectLabels(pid interface{}) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()
	labels := GetProjectLabels(gl, pid)

	// output the data
	table := DefaultTable()
	table.AddRow("NAME", "DESCRIPTION")
	for _, label := range labels {
		table.AddRow(label.Name, label.Description)
	}
	fmt.Println(table)
}

func CreateProjectLabel(pid interface{}, title, color string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	opts := &gitlab.CreateLabelOptions{}
	opts.Name = gitlab.String(title)
	opts.Color = gitlab.String(color)
	label, _, err := gl.Labels.CreateLabel(pid, opts)
	fatalOnErr(err)
	fmt.Println("Label created:", label.Name)
}

func EditProjectLabel(pid interface{}, title, newTitle, newColor, newDesc string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	opts := &gitlab.UpdateLabelOptions{}
	opts.Name = gitlab.String(title)
	opts.NewName = gitlab.String(newTitle)
	opts.Color = gitlab.String(newColor)
	opts.Description = gitlab.String(newDesc)
	_, _, err := gl.Labels.UpdateLabel(pid, opts)
	fatalOnErr(err)
	fmt.Println("Label updated")
}

func DeleteProjectLabel(pid interface{}, title string) {
	if pid.(string) == "" {
		pid = GetDefaultPID()
	}
	log.Println("pid:", pid)

	gl := GetGitlabInstance()

	opts := &gitlab.DeleteLabelOptions{}
	opts.Name = gitlab.String(title)
	_, err := gl.Labels.DeleteLabel(pid, opts)
	fatalOnErr(err)
	fmt.Println("Label Deleted:", title)
}

func GetProjectLabels(gl *gitlab.Client, pid interface{}) []*gitlab.Label {
	labels, _, err := gl.Labels.ListLabels(pid)
	if err != nil {
		log.Fatalln("error in getting the list of project labels")
	}
	return labels
}
